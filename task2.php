<?php

$hours = rand(0, 23);
$minutes = rand(0, 59);
$seconds = rand(0, 59);

$formattedHours = $hours;
$label = 'AM';

if ($hours > 11) {
    $label = 'PM';
}

if ($hours > 12) {
    $formattedHours -= 12;
}

echo str_pad($formattedHours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':' . str_pad($seconds, 2, '0', STR_PAD_LEFT) . " $label";

$addedSeconds = rand(0, 60);
$seconds += $addedSeconds;
if ($seconds > 59) {
    $seconds -= 60;
    $minutes++;
    if ($minutes > 59) {
        $minutes -= 60;
        $hours++;
        if ($hours > 23) {
            $hours -= 24;
            $formattedHours = $hours;
            $label = 'AM';
            if ($hours > 11) {
                $label = 'PM';
            }

            if ($hours > 12) {
                $formattedHours -= 12;
            }
        }
    }
}

echo "<br> added $addedSeconds seconds";
echo "<br> new time is ";

echo str_pad($formattedHours, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':' . str_pad($seconds, 2, '0', STR_PAD_LEFT) . " $label";
